# Required software
* Ruby, Bundler, Firefox 45.4.0

##Installation instructions

### Ruby
* To check if you already have ruby installed, you can check your version by running this command
```
ruby --version
```
* We recommend you use at least version 2.0.0
* If you need to install Ruby you can go to this website and follow instructions for your OS
    - https://www.ruby-lang.org/en/documentation/installation/


### Bundler
* Install Bundler:
```
gem install bundler
```
* Go to your project and install all needed gems:
```
cd path_to_project
bundle install
```

### Firefox
* To install Firefox you can go to their website and get the correct version (45.4.0)
    - https://ftp.mozilla.org/pub/firefox/releases/


#Tasks

##Task 1

###Fix and complete the 2 current tests

### Username and password
* You can create a user account for free at https://admin.typeform.com
* Once created you can adjust the variables in `step_defs.rb` and tests will run using your account
* DO NOT submit your username and password as part of your answer

##Task 2

###Improve the test suite where possible (without adding further tests)
