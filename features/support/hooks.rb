# FIREFOX_VERSION = '45.4.0'

Before do
  @browser = Selenium::WebDriver.for :firefox
  raise "Use the firefox version: #{FIREFOX_VERSION}" unless @browser.capabilities[:version].eql?(FIREFOX_VERSION)
  @browser.manage.window.maximize
end


After do
  @browser.close
end