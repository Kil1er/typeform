def ajax_active?
  @browser.execute_script("return $.active != 0")
end

def check_for_ajax(window = 1, global_timeout = 45)
  if global_timeout == 0
    raise "Timed out waiting for ajax requests to complete"
  end
  sleep window

  if ajax_active?
    check_for_ajax window, global_timeout-= 1
  else
    true
  end

end
