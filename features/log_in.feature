Feature: Typeform Hiring Exercise

  Scenario: Log In Successfully
    Given I try to log in as valid user
    Then I should be logged in


  Scenario: Log In Unsuccessfully
    Given I try to log in as invalid user
    Then I should get an error message