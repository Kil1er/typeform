require_relative '../../features/support/helpers/check_ajax'

When(/^I try to log in (.*) user$/) do |user_type|
  user = data_load[user_type.to_sym]
  check_for_ajax
  @browser.get 'https://admin.typeform.com/login/'
  @browser.find_element(:name, '_username').send_keys(user[:username])
  @browser.find_element(:name, '_password').send_keys(user[:password])
  @browser.find_element(:id, 'btnlogin').click
  check_for_ajax
end

Then(/^I should be logged in$/) do
  expect(@browser.find_element(:id, 'header-logotype').displayed?).to eql(true)
end

Then /^I should get an error message$/ do
  expect(@browser.find_element(:id, 'error').displayed?).to be_truthy
end